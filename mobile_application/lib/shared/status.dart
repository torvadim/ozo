import 'package:flutter/material.dart';

class StatusComponent extends StatelessWidget {
  int _status;
  Color _color = Colors.white;

  StatusComponent(this._status);

  StatusComponent.withColor(this._status, this._color);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
        child: Column(
          children: [
            Text(_status.toString(),
                style: TextStyle(color: _color, fontSize: 12.0)),
            Image(
              image: AssetImage("assets/icons/status.png"),
              width: 25,
              height: 25,
              color: _color,
            )
          ],
        ));
  }
}
