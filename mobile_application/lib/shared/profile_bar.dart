import 'package:flutter/material.dart';
import 'package:ozo/models/author.dart';
import 'package:intl/intl.dart';

import "./status.dart";

class ProfileBar extends StatelessWidget {
  final Author _author;
  final String _date;
  final int _status;

  ProfileBar(this._author, this._date, this._status);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Card(
            child: Column(
          children: [
            ListTile(
                title: Text(_author.name),
                subtitle: Text(
                    DateFormat('yyyy-MM-dd').format(DateTime.parse(_date)),
                    style: TextStyle(
                        fontWeight: FontWeight.w300, color: Colors.black)),
                leading: Image.network(_author.avatar, height: 50),
                trailing: StatusComponent.withColor(_status, Colors.black)),
          ],
        )),
        onTap: () => Navigator.pushNamed(context, '/profile',
            arguments: {"profileId": _author.id}));
  }
}
