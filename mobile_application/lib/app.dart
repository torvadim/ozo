import 'package:flutter/material.dart';
import 'package:ozo/screens/profile/profile.dart';
import 'screens/question/question.dart';
import 'package:ozo/screens/feed/feed.dart';
import 'package:ozo/screens/categories/categories.dart';
import 'package:ozo/screens/categories/categoryFeed.dart';
import 'package:ozo/screens/about/about.dart';

import 'style.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: new Feed(),
      onGenerateRoute: _routes(),
      theme: _theme(),
    );
  }

  RouteFactory _routes() {
    return (RouteSettings settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;
      switch (settings.name) {
        case '/feed':
          screen = Feed();
          break;
        case '/profile':
          screen = Profile(arguments['profileId']);
          break;
        case '/question':
          screen = QuestionPage(arguments['questionId']);
          break;
        case '/categories':
          screen = Categories();
          break;
        case '/categoryFeed':
          screen = CategoryFeed(arguments['category']);
          break;
        case '/about':
          screen = AboutInfo();
          break;
        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }

  ThemeData _theme() {
    return ThemeData(
      appBarTheme: AppBarTheme(textTheme: TextTheme(title: AppBarTextStyle)),
      textTheme: TextTheme(
        title: TitleTextStyle,
        subtitle: SubTitleTextStyle,
        caption: CaptionTextStyle,
        body1: Body1TextStyle,
      ),
      primaryColor: Colors.black,
      accentColor: Colors.green,
    );
  }
}
