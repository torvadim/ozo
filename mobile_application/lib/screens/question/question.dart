import 'package:flutter/material.dart';
import 'package:ozo/models/answer.dart';
import 'package:ozo/screens/question/answer_card.dart';
import 'package:ozo/screens/question/text_section.dart';
import 'image_banner.dart';
import "../../shared/profile_bar.dart";
import '../../models/question.dart';

class QuestionPage extends StatelessWidget {
  final String _questionID;

  QuestionPage(this._questionID);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle =
        theme.textTheme.headline.copyWith(color: Colors.white);

    return Scaffold(
        appBar: AppBar(
          title: Text('Question'),
        ),
        body: new FutureBuilder<Question>(
            future: Question().fetchQuestion(_questionID),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                Question question = snapshot.data;
                return ListView(
                  children: [
                    Card(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black, width: 3),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        clipBehavior: Clip.antiAlias,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: 184.0,
                              child: Stack(
                                children: <Widget>[
                                  Positioned.fill(
                                    child: ImageBanner(question.image),
                                  ),
                                  Positioned(
                                    bottom: 16.0,
                                    left: 16.0,
                                    right: 16.0,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        question.header,
                                        style: titleStyle,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            ProfileBar(question.author, question.created,
                                question.status),
                            TextSection(question.description),
                          ],
                        )),
                    Column(children: answerBuilder(question.answers))
                  ],
                );
              } else if (snapshot.hasError) {
                return new Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return new Center(child: CircularProgressIndicator());
            }));
  }

  List<Widget> answerBuilder(List<Answer> answers) {
    List<Widget> answersList = [];
    for (int i = 0; i < answers.length; i++) {
      answersList.add(AnswerCard(answers[i]));
    }
    return answersList;
  }
}
