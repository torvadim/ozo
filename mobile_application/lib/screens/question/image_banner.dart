import 'package:flutter/material.dart';
import '../../constants.dart' as Constants;

class ImageBanner extends StatelessWidget {
  final String _imageUrl;

  ImageBanner(this._imageUrl);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(height: 250.0),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                Constants.API_URL + 'img/' + _imageUrl,
              ),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(Colors.black38, BlendMode.darken))),
    );
  }
}
