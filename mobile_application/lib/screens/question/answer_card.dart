import 'package:flutter/material.dart';
import 'package:ozo/shared/profile_bar.dart';
import '../../models/answer.dart';
import "./text_section.dart";

class AnswerCard extends StatelessWidget {
  final Answer _answer;

  AnswerCard(this._answer);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ProfileBar(_answer.author, _answer.created, _answer.status),
          TextSection(_answer.text)
        ],
      ),
    );
  }
}
