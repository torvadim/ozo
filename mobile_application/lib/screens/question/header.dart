import 'package:flutter/material.dart';
import '../../models/author.dart';

class Header extends StatelessWidget {
  final String _header;

  var author = Author().fetchAuthor('20a21e8c-6e55-4b59-9ba9-88050ecb4b9f');

  Header(this._header);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 16.0, 16, 16.0),
      child: Text(_header, style: Theme.of(context).textTheme.headline),
    );
  }
}
