import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  final String _description;

  TextSection(this._description);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
      child: Text(
        _description,
        style: Theme.of(context).textTheme.body1,
      ),
    );
  }
}
