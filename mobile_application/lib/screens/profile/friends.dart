import 'package:flutter/material.dart';
import 'package:ozo/models/author.dart';

class Friends extends StatelessWidget {
  List _friendsIds;

  Friends(this._friendsIds);

  Widget _itemBuilder(BuildContext context, String friendId) {
    return new FutureBuilder(
      future: Author().fetchAuthor(friendId),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
              width: 45,
              padding: const EdgeInsets.fromLTRB(0, 0, 5, 10),
              child: InkWell(
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(
                      snapshot.data.avatar,
                    ),
                  ),
                  onTap: () => Navigator.pushNamed(context, '/profile',
                      arguments: {"profileId": friendId})));
        }

        return Container(width: 0.0, height: 0.0);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = 0;
    if (_friendsIds.length > 0) height = 50;
    return Container(
        alignment: Alignment.center,
        height: height,
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: _friendsIds.length,
          itemBuilder: (context, index) {
            return _itemBuilder(context, _friendsIds[index]);
          },
        ));
  }
}
