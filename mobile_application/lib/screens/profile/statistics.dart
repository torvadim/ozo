import 'package:flutter/material.dart';
import 'package:ozo/models/author.dart';
import "package:ozo/shared/status.dart";

class Statistics extends StatelessWidget {
  final Stats _stats;

  Statistics(this._stats);

  Widget questionsAndAnswers(property, value) {
    return Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(property, style: TextStyle(fontSize: 12.0)),
          Text(value.toString(), style: TextStyle(fontSize: 12.0))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            questionsAndAnswers('questions', _stats.questions),
            questionsAndAnswers('answers', _stats.answers)
          ],
        ),
        StatusComponent.withColor(_stats.total, Colors.black)
      ],
    ));
  }
}
