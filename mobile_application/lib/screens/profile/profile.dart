import 'package:flutter/material.dart';
import 'package:ozo/models/author.dart';
import 'package:ozo/screens/profile/categories.dart';
import 'package:ozo/screens/feed/feed_component.dart';
import 'package:ozo/screens/profile/friends.dart';
import 'package:ozo/screens/profile/statistics.dart';

class Profile extends StatelessWidget {
  final String _id;

  Profile(this._id);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle = theme.textTheme.headline5;

    return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
        ),
        body: new FutureBuilder<Author>(
          future: Author().fetchAuthor(_id),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              Author author = snapshot.data;
              return ListView(
                children: [
                  Card(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.black, width: 3),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      clipBehavior: Clip.antiAlias,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          SizedBox(
                            height: 184.0,
                            child: Stack(
                              children: <Widget>[
                                Positioned.fill(
                                  child: Container(
                                      constraints:
                                          BoxConstraints.expand(height: 250.0),
                                      child: Image.network(
                                        author.avatar,
                                        fit: BoxFit.cover,
                                      )),
                                ),
                              ],
                            ),
                          ),
                          Container(
                              padding: const EdgeInsets.all(10.0),
                              alignment: Alignment.center,
                              child: Text(author.name, style: titleStyle)),
                          Statistics(snapshot.data.stats),
                          Friends(snapshot.data.friends),
                          Categories(snapshot.data.categories),
                        ],
                      )),
                  FeedComponent.withAuthor(author.id)
                ],
              );
            } else if (snapshot.hasError) {
              return new Text("${snapshot.error}");
            }

            // By default, show a loading spinner
            return new CircularProgressIndicator();
          },
        ));
  }
}
