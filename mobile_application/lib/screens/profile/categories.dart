import 'package:flutter/material.dart';
import 'package:ozo/models/author.dart';

class Categories extends StatelessWidget {
  final List _categories;

  Categories(this._categories);

  Widget _itemBuilder(BuildContext context, String category) {
    return Container(
        padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
        child: InkWell(
            child: Container(
                child: Text(category,
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ))),
            onTap: () => Navigator.pushNamed(context, '/categoryFeed',
                arguments: {"category": category})));
  }

  @override
  Widget build(BuildContext context) {
    double height = 0;
    if (_categories.length > 0) height = 40;
    return Container(
        alignment: Alignment.center,
        height: height,
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: _categories.length,
          itemBuilder: (context, index) {
            return _itemBuilder(context, _categories[index]);
          },
        ));
  }
}
