import 'package:flutter/material.dart';
import 'feed_component.dart';

class Feed extends StatelessWidget {
  @override
  build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feed'),
      ),
      drawer: new Drawer(
          child: new ListView(
        children: <Widget>[
          new DrawerHeader(
            child: Image(image: AssetImage("assets/OZO-logo.png")),
          ),
          new ListTile(
            title: new Text('Questions'),
            onTap: () {
              Navigator.pushNamed(context, "/feed");
            },
          ),
          new ListTile(
            title: new Text('Categories'),
            onTap: () {
              Navigator.pushNamed(context, "/categories");
            },
          ),
          new Divider(),
          new ListTile(
            title: new Text('About'),
            onTap: () {
              Navigator.pushNamed(context, "/about");
            },
          ),
        ],
      )),
      body: FeedComponent(),
    );
  }
}
