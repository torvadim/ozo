import 'package:flutter/material.dart';
import 'package:ozo/models/author.dart';
import 'package:intl/intl.dart';

import "../../shared/status.dart";
import '../../constants.dart' as Constants;

class QuestionCard extends StatelessWidget {
  final String id;
  final String _header;
  final Author _author;
  final int _status;
  final String _imageUrl;
  final String _created;

  QuestionCard(this.id, this._header, this._author, this._status,
      this._imageUrl, this._created);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => _onQuestionTap(context, id),
        child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                      Constants.API_URL + 'img/' + _imageUrl,
                    ),
                    fit: BoxFit.cover,
                    colorFilter:
                        ColorFilter.mode(Colors.black38, BlendMode.darken))),
            child: Container(
                padding: const EdgeInsets.fromLTRB(16, 32.0, 16, 4.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(_author.name,
                        style: Theme.of(context).textTheme.caption),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 4.0),
                    ),
                    Text(_header, style: Theme.of(context).textTheme.title),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                              DateFormat('yyyy-MM-dd')
                                  .format(DateTime.parse(_created)),
                              style: Theme.of(context).textTheme.caption),
                          StatusComponent(_status)
                        ])
                  ],
                ))));
  }

  _onQuestionTap(BuildContext context, String questionID) {
    Navigator.pushNamed(context, '/question',
        arguments: {"questionId": questionID});
  }
}
