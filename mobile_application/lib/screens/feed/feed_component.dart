import 'package:flutter/material.dart';
import 'package:ozo/models/question.dart';
import 'package:ozo/screens/feed/question_card.dart';
import 'package:ozo/screens/feed/placeholder.dart';

class FeedComponent extends StatefulWidget {
  String author = "";
  String category = "";

  FeedComponent();

  FeedComponent.withAuthor(this.author);
  FeedComponent.withCategory(this.category);

  createState() => FeedState(author, category);
}

class FeedState extends State<FeedComponent> {
  String author = "";
  String category = "";

  FeedState(this.author, this.category);
  ScrollController controller;
  List<Question> questions = <Question>[];
  int page = 0;

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    isLoading = !isLoading;
    startLoader();

    controller = new ScrollController()..addListener(_scrollListener);
  }

  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      startLoader();
    }
  }

  void startLoader() {
    setState(() {
      isLoading = true;
      fetchData();
    });
  }

  fetchData() {
    setState(() {
      page += 1;
      Question().fetchFeed(author, category, false, page).then((value) {
        setState(() {
          questions.addAll(value);
          isLoading = false;
        });
      });
    });
  }

  Widget _loader() {
    return isLoading
        ? new Align(
            child: new Container(
              width: 70.0,
              height: 70.0,
              child: new Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: new Center(child: new CircularProgressIndicator())),
            ),
            alignment: FractionalOffset.bottomCenter,
          )
        : new SizedBox(
            width: 0.0,
            height: 0.0,
          );
  }

  Widget build(BuildContext context) {
    getData() {
      if (questions.length == 0 && author == "" && !isLoading) {
        return PlaceHolder();
      }
      return Flexible(
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: questions.length,
          controller: controller,
          itemBuilder: (context, index) =>
              _itemBuilder(context, questions[index]),
        ),
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [getData(), _loader()],
    );
  }

  Widget _itemBuilder(BuildContext context, Question question) {
    return QuestionCard(question.id, question.header, question.author,
        question.status, question.image, question.created);
  }
}
