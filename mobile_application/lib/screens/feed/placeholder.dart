import 'package:flutter/material.dart';

class PlaceHolder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      heightFactor: 30,
      child: Text("Suddenly, there aren't any questions yet!"),
    );
  }
}
