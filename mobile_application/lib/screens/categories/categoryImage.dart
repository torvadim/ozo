import 'dart:ui';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

class CategoryImage extends StatelessWidget {
  final String _category;

  CategoryImage(this._category);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.pushNamed(context, '/categoryFeed',
            arguments: {"category": _category}),
        child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image:
                        AssetImage("assets/categories/" + _category + ".jpeg"),
                    fit: BoxFit.cover,
                    colorFilter:
                        ColorFilter.mode(Colors.black12, BlendMode.darken))),
            child: Container(
                padding: EdgeInsets.all(8.0), child: Text(toBeginningOfSentenceCase(_category)))));
  }
}
