import 'package:flutter/material.dart';
import 'categoryImage.dart';
import '../../constants.dart' as Constants;

class Categories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Feed'),
        ),
        body: Column(children: [
          Expanded(
              child: GridView.count(
            crossAxisCount: 2,
            mainAxisSpacing: 8.0,
            crossAxisSpacing: 8.0,
            padding: EdgeInsets.all(8.0),
            children: getCategories(Constants.CATEGORIES),
          ))
        ]));
  }
}

List<Widget> getCategories(categories) {
  List<Widget> cats = new List<Widget>();

  for (var i = 0; i < categories.length; i++) {
    cats.add(new CategoryImage(categories[i]));
  }
  return cats;
}
