import 'package:flutter/material.dart';
import "package:ozo/screens/feed/feed_component.dart";
import 'package:intl/intl.dart';

class CategoryFeed extends StatelessWidget {
  final String _category;

  CategoryFeed(this._category);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(toBeginningOfSentenceCase(_category)),
        ),
        body: FeedComponent.withCategory(_category));
  }
}
