import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'author.dart';
import '../constants.dart' as Constants;

class Answer {
  final String id;
  final Author author;
  final String text;
  final int status;
  final String created;
  final String updated;

  Answer(
      {this.id,
      this.author,
      this.text,
      this.status,
      this.created,
      this.updated});

  Future<List<Answer>> fetchAnswers(String id) async {
    final response =
        await http.get(Constants.API_URL + 'questions/' + id + '/answers');
    var responseJson = jsonDecode(response.body.toString());

    return createAnswerList(responseJson);
  }
}

List<Answer> createAnswerList(List data) {
  List<Answer> list = new List();

  for (int i = 0; i < data.length; i++) {
    String id = data[i]["id"];
    String text = data[i]["text"];
    int status = data[i]["status"];
    String created = data[i]["created"];
    String updated = data[i]["updated"];

    var author = Author.inFeed(
        id: data[i]["author"]["id"],
        name: data[i]["author"]["name"],
        avatar: data[i]["author"]["avatar"]);

    Answer answer = new Answer(
        id: id,
        author: author,
        text: text,
        status: status,
        created: created,
        updated: updated);
    list.add(answer);
  }

  return list;
}
