import 'package:http/http.dart' as http;
import 'author.dart';
import 'answer.dart';
import 'dart:async';
import 'dart:convert';
import '../constants.dart' as Constants;

class Question {
  final String id;
  final Author author;
  final String header;
  final String description;
  final String image;
  List<Answer> answers;
  final int status;
  List categories;
  final String created;
  String updated;

  Question(
      {this.id,
      this.author,
      this.header,
      this.description,
      this.image,
      this.answers,
      this.status,
      this.categories,
      this.created,
      this.updated});

  Question.forFeed(this.id, this.author, this.header, this.description,
      this.image, this.status, this.created);

  Future<Question> fetchQuestion(String id) async {
    final response = await http.get(Constants.API_URL + 'questions/' + id);
    var responseJson = jsonDecode(response.body.toString());

    var author = Author.inFeed(
        id: responseJson["author"]["id"],
        name: responseJson["author"]["name"],
        avatar: responseJson["author"]["avatar"]);

    List<Answer> answers = await Answer().fetchAnswers(id);

    return Question(
      id: responseJson["id"],
      author: author,
      header: responseJson["header"],
      description: responseJson["description"],
      image: responseJson["image"],
      answers: answers,
      status: responseJson["status"],
      categories: responseJson["categories"],
      created: responseJson["created"],
      updated: responseJson["updated"],
    );
  }

  Future<List<Question>> fetchFeed(
      String author, String category, byStatus, int page) async {
    var query = "?page=" + page.toString() + "&limit=10";
    if (author != "") query += "&author=" + author;
    if (category != "") query += "&category=" + category;
    if (byStatus) query += '&byStatus=true';

    final response = await http.get(Constants.API_URL + 'questions' + query);

    var responseJson = jsonDecode(response.body.toString());

    return createQuestionsList(responseJson);
  }
}

List<Question> createQuestionsList(List data) {
  List<Question> list = new List();

  for (int i = 0; i < data.length; i++) {
    String id = data[i]["id"];
    String header = data[i]["header"];
    String description = data[i]["description"];
    String image = data[i]["image"];
    int status = data[i]["status"];
    String created = data[i]["created"];

    var author = Author.inFeed(
        id: data[i]["author"]["id"],
        name: data[i]["author"]["name"],
        avatar: data[i]["author"]["avatar"]);

    Question question = new Question(
      id: id,
      author: author,
      header: header,
      image: image,
      description: description,
      status: status,
      created: created,
    );

    list.add(question);
  }

  return list;
}
