import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../constants.dart' as Constants;

class Author {
  final String id;
  final String name;
  final String avatar;
  List friends;
  List categories;
  var stats;
  String created;

  Author(
      {this.id,
      this.name,
      this.avatar,
      this.friends,
      this.categories,
      this.stats,
      this.created});

  Author.inFeed({this.id, this.name, this.avatar});

  Future<Author> fetchAuthor(String id) async {
    final response = await http.get(Constants.API_URL + 'users/' + id);
    var responseJson = jsonDecode(response.body.toString());

    return Author(
      id: responseJson['id'] as String,
      name: responseJson['name'] as String,
      avatar: responseJson['avatar'] as String,
      friends: responseJson['friends'] as List,
      categories: responseJson['categories'] as List,
      stats: new Stats.fromJson(responseJson['stats']),
      created: responseJson['created'] as String,
    );
  }
}

class Stats {
  int questions;
  int answers;
  int total;

  Stats({this.questions, this.answers, this.total});

  Stats.fromJson(Map<String, dynamic> json) {
    questions = json['questions'];
    answers = json['answers'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['questions'] = this.questions;
    data['answers'] = this.answers;
    data['total'] = this.total;
    return data;
  }
}
