library constants;

const String API_URL = 'http://ozo-api.azurewebsites.net/';

const List<String> CATEGORIES = [
  "art",
  "culture",
  "economics",
  "history",
  "human",
  "internet",
  "science",
  "sports"
];
