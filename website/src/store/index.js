import Vue from "vue";
import Vuex from "vuex";
import feed from "./modules/feed";
import question from "./modules/question";
import status from "./modules/status";
import profile from "./modules/profile";

import jwtDecode from "jwt-decode";

import { i18n } from "../locales/i18n";
import { apiUrl } from "./../constants/urls";
import router from "../router";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export const actions = {
  logout({ commit }) {
    commit("logout");
    localStorage.removeItem("token");
    localStorage.removeItem("user");

    delete Vue.axios.defaults.headers.common.Authorization;
  },
  async getUser({ commit, state }, refresh) {
    Vue.axios.defaults.headers.common.Authorization = "Bearer " + state.token;

    if ((state.token && !state.user.id) || refresh) {
      const { userId, isAdmin } = jwtDecode(state.token);
      const { data: user } = await Vue.axios.get(`${apiUrl}/users/${userId}`);

      commit("setUser", { ...user, isAdmin });
    }
  },
  async uploadImage(state, { formData }) {
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };

    const { data: response } = await Vue.axios.post(
      `${apiUrl}/img`,
      formData,
      config
    );
    return response;
  },
  async sendQuestion(state, question) {
    const { data: response } = await Vue.axios.post(
      `${apiUrl}/questions/`,
      question
    );

    Vue.notify({
      group: "app",
      type: "success",
      title: i18n.t("notifications.sentQuestions")
    });

    router.push({ name: "question", params: { id: response.id } });
  },
  async changeLanguage({ commit }, lang) {
    await import(`@/locales/lang/${lang}.json`)
      .then(imports => {
        i18n.setLocaleMessage(lang, imports.default || imports);
        i18n.locale = lang;
        commit("setLanguage", lang);
        localStorage.setItem("lang", lang);
      })
      .catch(() => {
        console.log("Requested locale was not found");
      });
  }
};

export const mutations = {
  setToken(state, token) {
    state.token = token;
  },
  logout(state) {
    state.token = "";
    state.user = {};
  },
  setUser(state, user) {
    state.user = user;
  },
  setShowLoginModal(state, show) {
    state.showLoginModal = show;
  },
  setShowAskModal(state, show) {
    state.showAskModal = show;
  },
  setLoading(state, isLoading) {
    state.isLoading = isLoading;
  },
  setLanguage(state, lang) {
    state.lang = lang;
  }
};

export default new Vuex.Store({
  namespaced: true,
  state: {
    token: "",
    user: {},
    lang: "",
    showLoginModal: false,
    showAskModal: false,
    isLoading: false
  },
  actions,
  mutations,

  modules: {
    feed,
    question,
    status,
    profile
  },
  strict: debug
});
