import Vue from "vue";
import { apiUrl } from "./../../constants/urls";

const actions = {
  async voteUp({ commit }, { questionId, answerId }) {
    let url = `${apiUrl}/questions/${questionId}`;
    if (answerId) url += `/answers/${answerId}`;
    const res = await Vue.axios.post(`${url}/up`);
    if (answerId) {
      commit(
        "question/updateAnswerStatus",
        { answerId, status: res.data },
        { root: true }
      );
      return;
    }
    await commit("question/updateQuestionStatus", res.data, { root: true });
  },

  async voteDown({ commit }, { questionId, answerId }) {
    let url = `${apiUrl}/questions/${questionId}`;
    if (answerId) url += `/answers/${answerId}`;
    const res = await Vue.axios.post(`${url}/down`);
    if (answerId) {
      commit(
        "question/updateAnswerStatus",
        { answerId, status: res.data },
        { root: true }
      );
      return;
    }
    await commit("question/updateQuestionStatus", res.data, { root: true });
  }
};

export default {
  namespaced: true,
  actions
};
