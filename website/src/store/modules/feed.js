import Vue from "vue";
import { apiUrl } from "./../../constants/urls";

export function getDefaultState() {
  return {
    items: [],
    page: 1,
    questionCount: 11,
    perPage: 11
  };
}

const state = getDefaultState();

export const actions = {
  async loadQuestions(
    { commit, state },
    { author, category, byStatus, reset }
  ) {
    let url = `${apiUrl}/questions?page=${reset ? 1 : state.page}&limit=${
      state.perPage
    }`;

    if (author) url += `&author=${author}`;
    if (category) url += `&category=${category}`;
    url += `&byStatus=${byStatus || false}`;

    const { data: items, headers } = await Vue.axios.get(url);

    if (headers.ozo_pagination_total) {
      commit("setQuestionCount", Number(headers.ozo_pagination_total));
    }

    if (items.length) {
      commit("setQuestions", { items, reset });
    } else {
      commit("resetQuestions");
    }
  },

  async loadPersonalFeed({ commit, state }, { byStatus, reset }) {
    let url = `${apiUrl}/feed?page=${reset ? 1 : state.page}&limit=${
      state.perPage
    }`;
    url += `&byStatus=${byStatus || false}`;
    const { data: items, headers } = await Vue.axios.get(url);

    if (headers.ozo_pagination_total) {
      commit("setQuestionCount", Number(headers.ozo_pagination_total));
    }

    if (items.length) {
      commit("setQuestions", { items, reset });
    } else {
      commit("resetQuestions");
    }
  }
};

export const mutations = {
  setQuestions(state, { items, reset }) {
    if (reset) {
      state.items = items;
      state.page = 2;
    } else {
      state.page++;
      state.items.push(...items);
    }
  },
  setQuestionCount(state, count) {
    state.questionCount = count;
  },
  resetQuestions(state) {
    state.items = [];
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
