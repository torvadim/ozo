import Vue from "vue";

import { apiUrl } from "../../constants/urls";

export function getDefaultState() {
  return {
    id: "",
    name: "Loading...",
    avatar: "",
    stats: {
      questions: 0,
      answers: 0,
      total: 0
    },
    friends: [],
    categories: []
  };
}

const state = getDefaultState();

export const actions = {
  async loadUserProfile({ commit, dispatch }, id) {
    const { data: user } = await Vue.axios.get(`${apiUrl}/users/${id}`);

    commit("setProfileInfo", { user });
    dispatch("loadFriendsList", user.friends);
  },
  loadFriendsList(state, friends) {
    const friendsOfUser = friends;

    if (friendsOfUser) {
      for (let i = 0; i < friendsOfUser.length; i++) {
        Vue.axios
          .get(`${apiUrl}/users/${friendsOfUser[i]}`)
          .then(responseFriend => {
            const friend = responseFriend.data;
            state.commit("setProfileFriends", friend);
          });
      }
    }
  },
  async addToFriendsList(store, { friend, add }) {
    await Vue.axios.patch(`${apiUrl}/users/${friend}?add=${add}`);
  },
  async subscribeToCat(store, { cat }) {
    await Vue.axios.patch(`${apiUrl}/users?category=${cat}`);
  },
  resetStateAction({ commit }) {
    commit("resetProfileState");
  }
};

export const mutations = {
  setProfileInfo(state, { user }) {
    state.id = user.id;
    state.name = user.name;
    state.avatar = user.avatar;
    state.categories = user.categories;
    state.stats = user.stats;
  },
  setProfileFriends(state, friend) {
    state.friends.push(friend);
  },
  resetProfileState(state) {
    Object.assign(state, getDefaultState());
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
