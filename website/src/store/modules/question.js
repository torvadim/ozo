import router from "../../router";
import Vue from "vue";
import { i18n } from "../../locales/i18n";
import { apiUrl } from "./../../constants/urls";

const state = {
  question: {
    header: "Loading...",
    author: {
      avatar: "@/assets/OZO-logo-medium.png",
      name: "Loading..."
    }
  },
  answers: []
};

export const actions = {
  async loadQuestion({ commit, dispatch }, questionId) {
    const { data: questionData } = await Vue.axios.get(
      `${apiUrl}/questions/${questionId}`
    );
    commit("setQuestion", questionData);
    dispatch("loadAnswers", questionData.id);
  },

  async loadAnswers({ commit }, questionId) {
    const { data: answers } = await Vue.axios.get(
      `${apiUrl}/questions/${questionId}/answers`
    );

    commit("setAnswers", answers);
  },

  async sendAnswer({ dispatch }, { questionId, answer }) {
    await Vue.axios.post(`${apiUrl}/questions/${questionId}/answers`, {
      text: answer
    });

    dispatch("loadAnswers", questionId);
  },

  async updateQuestion({ dispatch }, { question, id }) {
    const { data: result } = await Vue.axios.patch(
      `${apiUrl}/questions/${id}`,
      question
    );

    Vue.notify({
      group: "app",
      type: "success",
      title: i18n.t("question.updated")
    });

    dispatch("loadQuestion", result.id);
  },

  async removeQuestion(state, id) {
    await Vue.axios.delete(`${apiUrl}/questions/${id}`);

    router.push({ name: "personalFeed" });
    Vue.notify({
      group: "app",
      type: "success",
      title: i18n.t("question.updated")
    });
  },

  async updateAnswer({ dispatch }, { text, questionId, answerId }) {
    await Vue.axios.patch(
      `${apiUrl}/questions/${questionId}/answers/${answerId}`,
      { text }
    );

    Vue.notify({
      group: "app",
      type: "success",
      title: i18n.t("answer.updated")
    });

    dispatch("loadQuestion", questionId);
  },

  async removeAnswer({ dispatch }, { questionId, answerId }) {
    await Vue.axios.delete(
      `${apiUrl}/questions/${questionId}/answers/${answerId}`
    );

    Vue.notify({
      group: "app",
      type: "success",
      title: i18n.t("answer.updated")
    });

    dispatch("loadQuestion", questionId);
  }
};

export const mutations = {
  setQuestion(state, question) {
    state.question = question;
  },

  setAnswers(state, answersData) {
    state.answers = answersData;
  },

  updateQuestionStatus(state, statusValue) {
    state.question.status = statusValue;
  },

  updateAnswerStatus(state, { answerId, status }) {
    try {
      state.answers.find(({ id }) => id === answerId).status = status;
    } catch (err) {
      console.log("Answer is not found");
    }
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
