import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueAxios from "vue-axios";
import Notifications from "vue-notification";
import { i18n } from "./locales/i18n";

import "./registerServiceWorker";

Vue.use(Notifications);

const queryString = window.location.search;
const params = new URLSearchParams(queryString);
const token = params.get("token");

if (token && !localStorage.getItem("token")) {
  localStorage.setItem("token", token);
  router.replace({ query: {} });
  router.go({ name: "questions" });
  axios.defaults.headers.common.Authorization = "Bearer " + token;
}

Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
