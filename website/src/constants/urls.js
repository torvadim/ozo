export const apiUrl = process.env.VUE_APP_API_URL
export const loginRedirectUrl = process.env.VUE_APP_LOGIN_REDIRECT_URL
