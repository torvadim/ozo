import Vue from "vue";
import Router from "vue-router";
import Feed from "./pages/feed.vue";
import PersonalFeed from "./pages/personalFeed";
import QuestionDetail from "./pages/question.vue";
import Profile from "./pages/profilePage";
import Redirect from "./pages/redirect";
import Categories from "./pages/categories";
import Rules from "./pages/rules";
import Policy from "./pages/policy";

import errorPage from "./pages/notFound";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  linkExactActiveClass: "exact-active",
  routes: [
    {
      path: "/r",
      name: "redirect",
      component: Redirect
    },
    {
      path: "/",
      name: "main",
      component: Feed
    },
    {
      path: "/feed",
      name: "personalFeed",
      component: PersonalFeed,
      props: true,
      beforeEnter: (to, from, next) => {
        if (!localStorage.getItem("token")) next("/questions");
        else next();
      }
    },
    {
      path: "/questions",
      name: "questions",
      component: Feed,
      props: true
    },
    {
      path: "/categories/:cat",
      name: "categories",
      component: Categories,
      props: true
    },
    {
      path: "/question/:id",
      name: "question",
      component: QuestionDetail,
      props: true
    },
    {
      path: "/profile/:id",
      name: "profile",
      component: Profile,
      props: true
    },
    {
      path: "/404",
      name: "errorPage",
      component: errorPage
    },
    {
      path: "/rules",
      name: "rules",
      component: Rules
    },
    {
      path: "/policy",
      name: "policy",
      component: Policy
    }
  ]
});
