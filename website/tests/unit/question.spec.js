import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import nock from "nock";
import { apiUrl } from "../../src/constants/urls";

Vue.use(VueAxios, axios);
import { mutations, actions } from "../../src/store/modules/question";

const {
  setQuestion,
  setAnswers,
  updateQuestionStatus,
  updateAnswerStatus
} = mutations;

const { loadQuestion, loadAnswers, sendAnswer } = actions;

describe("questions mutations", () => {
  it("Set question", () => {
    const state = {};

    setQuestion(state, { a: "b" });

    expect(state).toStrictEqual({ question: { a: "b" } });
  });

  it("Set answers", () => {
    const state = {};

    setAnswers(state, { a: "b" });

    expect(state).toStrictEqual({ answers: { a: "b" } });
  });

  it("Update question status", () => {
    const state = { question: { status: 10 } };

    updateQuestionStatus(state, 5);

    expect(state).toStrictEqual({ question: { status: 5 } });
  });

  it("Update answer status with right id", () => {
    const state = { answers: [{ id: "1", status: 10 }] };

    updateAnswerStatus(state, { answerId: "1", status: 15 });

    expect(state).toStrictEqual({ answers: [{ id: "1", status: 15 }] });
  });

  it("Update answer status with right id", () => {
    const state = { answers: [{ id: "1", status: 10 }] };

    updateAnswerStatus(state, { answerId: "2", status: 15 });

    expect(state).toStrictEqual({ answers: [{ id: "1", status: 10 }] });
  });
});

const testAction = async (action, payload, state, expectedActions) => {
  const commit = (type, payload) => {
    try {
      mutations[type](state, payload);
    } catch (err) {
      done(error);
    }
  };

  const dispatch = (type, payload) => {
    if (expectedActions.length) expect(expectedActions).toContain(type);
  };

  await action({ commit, dispatch, state }, payload);
};

describe("Questions actions", () => {
  it("Load question", async () => {
    nock(apiUrl)
      .get("/questions/10")
      .reply(200, { id: "10" });

    const state = {};

    await testAction(loadQuestion, 10, state, ["loadAnswers"]);

    expect(state).toStrictEqual({ question: { id: "10" } });
  });

  it("Load answers", async () => {
    nock(apiUrl)
      .get("/questions/10/answers")
      .reply(200, { id: "10" });

    const state = {};

    await testAction(loadAnswers, 10, state);

    expect(state).toStrictEqual({ answers: { id: "10" } });
  });

  it("Send answer", async () => {
    nock(apiUrl)
      .post("/questions/10/answers")
      .reply(200, {});

    await testAction(sendAnswer, { questionId: "10", answer: "a" }, {}, [
      "loadAnswers"
    ]);
  });
});
