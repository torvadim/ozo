import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import nock from "nock";
import { apiUrl } from "../../src/constants/urls";

Vue.use(VueAxios, axios);

import {
  mutations,
  actions,
  getDefaultState,
} from "../../src/store/modules/profile";

const { setProfileInfo, setProfileFriends, resetProfileState } = mutations;

const { loadUserProfile, addToFriendsList } = actions;

describe("Feed mutation", () => {
  it("setProfileInfo", () => {
    const state = {};
    const user = {
      id: "50957d67-7c6e-4832-9e8c-e2cca6d50021",
      name: "Test",
      avatar: "786a39c7-924c-4d68-91e0-bb5dc933cfb0",
      categories: ["a", "b", "c"],
      stats: {
        questions: 3,
        answers: 1,
        status: 4,
      },
    };

    setProfileInfo(state, { user });

    expect(state).toStrictEqual(user);
  });

  it("setProfileFriends", () => {
    const state = { friends: [] };

    setProfileFriends(state, { id: "424ea17d-6be6-43bc-bb7c-3dec55857967" });

    expect(state).toStrictEqual({
      friends: [{ id: "424ea17d-6be6-43bc-bb7c-3dec55857967" }],
    });
  });

  it("resetProfileState", () => {
    const state = {};

    resetProfileState(state);

    expect(state).toStrictEqual(getDefaultState());
  });
});

const testAction = async (action, payload, state, expectedActions) => {
  const commit = (type, payload) => {
    try {
      mutations[type](state, payload);
    } catch (err) {
      done(error);
    }
  };

  const dispatch = (type, payload) => {
    if (expectedActions.length) expect(expectedActions).toContain(type);
  };

  await action({ commit, dispatch, state }, payload);
};

const user = {
  id: "50957d67-7c6e-4832-9e8c-e2cca6d50021",
  name: "Test",
  avatar: "786a39c7-924c-4d68-91e0-bb5dc933cfb0",
  friends: [],
  categories: ["a", "b", "c"],
  stats: {
    questions: 3,
    answers: 1,
    status: 4,
  },
};

describe("Profile actions", () => {
  it("Load user profile ", async () => {
    nock(apiUrl).get("/users/a").reply(200, user);
    const state = getDefaultState();
    await testAction(loadUserProfile, "a", state, ["loadFriendsList"]);

    expect(state).toStrictEqual(user);
  });

  it("Add user to friend list", () => {
    nock(apiUrl).patch("/users/a?add=true").reply(200, {});

    testAction(addToFriendsList, { friend: "a", add: true });
  });
});
