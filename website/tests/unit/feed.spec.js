import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import nock from "nock";
import { apiUrl } from "../../src/constants/urls";

Vue.use(VueAxios, axios);

import { mutations, actions } from "../../src/store/modules/feed";

const { setQuestions, setQuestionCount, resetQuestions } = mutations;
const { loadQuestions, loadPersonalFeed } = actions;

describe("Feed mutation", () => {
  it("setQuestions mutations", () => {
    const state = { items: [], page: 0 };

    const expectedState = { items: ["a", "b", "c"] };

    setQuestions(state, expectedState);
    expect(state).toStrictEqual({ ...expectedState, page: 1 });
  });

  it("setQuestionCount mutation", () => {
    const state = { questionCount: 0 };

    setQuestionCount(state, 10);

    expect(state).toStrictEqual({ questionCount: 10 });
  });

  it("resetQuestions", () => {
    const state = { items: ["a", "b", "c"] };

    resetQuestions(state);

    expect(state).toStrictEqual({ items: [] });
  });
});

const testAction = async (action, payload, state, expectedActions) => {
  const commit = (type, payload) => {
    try {
      mutations[type](state, payload);
    } catch (err) {
      done(error);
    }
  };

  const dispatch = (type, payload) => {
    if (expectedActions.length) expect(expectedActions).toContain(type);
  };

  await action({ commit, dispatch, state }, payload);
};

describe("Feed actions", () => {
  it("Load question", async () => {
    nock(apiUrl)
      .get("/questions?page=0&limit=10&byStatus=false")
      .reply(200, ["a", "b"]);

    const state = { page: 0, perPage: 10, items: [] };

    await testAction(loadQuestions, {}, state);
    expect(state).toStrictEqual({ page: 1, perPage: 10, items: ["a", "b"] });
  });

  it("Load personal feed", async () => {
    nock(apiUrl)
      .get("/feed?page=0&limit=10&byStatus=false")
      .reply(200, ["a", "b"]);

    const state = { page: 0, perPage: 10, items: [] };

    await testAction(loadPersonalFeed, {}, state);
    expect(state).toStrictEqual({ page: 1, perPage: 10, items: ["a", "b"] });
  });
});
