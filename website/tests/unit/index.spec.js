import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import nock from "nock";
import { apiUrl } from "../../src/constants/urls";

Vue.use(VueAxios, axios);

import { mutations, actions } from "../../src/store/index";

const { logout, setUser, setShowLoginModal } = mutations;
const { getUser, changeLanguage } = actions;

describe("Global mutations", () => {
  it("Logout mutation clears state", () => {
    const state = {
      token: "aaa",
      user: {
        id: "f9816d24-df8d-43bd-816c-d86f7b9f564a"
      }
    };

    logout(state);
    expect(state).toStrictEqual({ token: "", user: {} });
  });

  it("getUser mutation set user object to the state", () => {
    const state = {};
    const user = { id: "84fb5ec2-9218-45ec-af55-cba2f037a14b" };

    setUser(state, user);

    expect(state).toStrictEqual({ user });
  });

  it("setShowLoginModal mutation set user object to the state", () => {
    const state = { showLoginModal: false };

    setShowLoginModal(state, true);

    expect(state).toStrictEqual({ showLoginModal: true });
  });
});

const testAction = async (action, payload, state, expectedActions) => {
  const commit = (type, payload) => {
    try {
      mutations[type](state, payload);
    } catch (err) {
      done(error);
    }
  };

  const dispatch = (type, payload) => {
    if (expectedActions.length) expect(expectedActions).toContain(type);
  };

  await action({ commit, dispatch, state }, payload);
};

describe("Main actions", () => {
  it("Get user data", async () => {
    nock(apiUrl)
      .get("/users/1234567890")
      .reply(200, { a: "b" });

    const state = {
      user: {},
      token:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxMjM0NTY3ODkwIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTUxNjIzOTAyMn0.SyrfLvB5DFKuSuEv0-36PjBRfy6LBnyVb6nWDJD1n3Q"
    };

    await testAction(getUser, {}, state);
    expect(state).toStrictEqual({
      ...state,
      user: { a: "b", isAdmin: false }
    });
  });

  it("Changing language", async () => {
    const state = { lang: "en" };

    await testAction(changeLanguage, "cz", state, ["setLanguage"]);
    expect(state).toStrictEqual({ lang: "cz" });
  });

  it("Changing language fails to load json file and do not change anything", async () => {
    const state = { lang: "en" };

    await testAction(changeLanguage, "cz", state);
    expect(state).toStrictEqual(state);
  });
});
