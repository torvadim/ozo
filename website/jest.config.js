module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  moduleNameMapper: {
    "/^@/(.*)$/": "src"
  },
  resolver: null,
  testEnvironment: "node"
};
