const constants = {
  VALID_JWT_TOKEN:
    'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMzQ0NjMyMC0yMzRjLTQzYjctYWY4ZS1hZGI5Y2FiNWZkNDciLCJ0b2tlbklkIjoiZDc5MTJlMzktZDY3MS00MGRhLWJjZjctYTU4YjBiOTZlZGQxIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTU2NjQxMTY2NH0.dkhpbgRsdVbpmcw7mxfRMKnS_9jpPij03d4E0YZfcEQ',
  AUTH_JWT_KEY: 'just-test-key',
  TOKEN_ID: 'd7912e39-d671-40da-bcf7-a58b0b96edd1',
  USER_ID: 'c3446320-234c-43b7-af8e-adb9cab5fd47',
  IS_ADMIN: false
}
module.exports = constants
