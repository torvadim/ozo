const axios = require('axios')
const constants = require('./constants')

const instance = axios.create()

instance.defaults.headers.common['access-control-request-headers'] =
  'authorization'
instance.defaults.headers.common['origin'] = 'https://localhost'

instance.interceptors.request.use(config => {
  config.headers.common['Authorization'] = constants.VALID_JWT_TOKEN
  return config
})

module.exports = instance
