const test = require('ava')
const uuid = require('uuid/v4')
const sinon = require('sinon')
const axios = require('./helpers/axios')
const server = require('../src/server')
const TransientMap = require('../src/TransientMap')
const constants = require('./helpers/constants')

test.beforeEach(async t => {
  let dbUrl = `mongodb://localhost:27017/test-${uuid()}`
  if (process.env.CI) dbUrl = `mongodb://mongo:27017/test-${uuid()}`

  const usersMap = new TransientMap()
  await usersMap.set(constants.USER_ID, {
    id: constants.USER_ID,
    tokenId: constants.TOKEN_ID,
    isAdmin: false
  })

  const config = {
    dbUrl,
    id: sinon.spy(uuid),
    now: sinon.spy(() => new Date()),
    usersMap,
    jwtSecret: constants.AUTH_JWT_KEY,
    auth: {
      gClientId: 'a',
      gClientSecret: 'b'
    }
  }

  const { db, httpServer } = await server(config)

  const requestUser = {
    _id: constants.USER_ID,
    name: 'Test user',
    avatar: 'some-picture',
    email: 'test@test.com',
    categories: [],
    friends: [],
    created: new Date(),
    updated: null
  }

  const user = {
    _id: uuid(),
    name: 'Test user',
    avatar: 'some-picture',
    email: 'test@test.com',
    categories: [],
    friends: [],
    created: new Date(),
    updated: null
  }

  await db.collection('users').insertOne(requestUser)

  t.context = {
    db,
    usersCol: db.collection('users'),
    requestUser,
    user,
    httpServer,
    url: `http://localhost:${httpServer.address().port}`,
    idSpy: config.id,
    nowSpy: config.now
  }
})

test.afterEach(async t => {
  let { db, httpServer } = t.context

  await Promise.all([
    db.dropDatabase(),
    new Promise(resolve => httpServer.close(resolve))
  ])
})

test('/users/:id GET returns profile of user', async t => {
  const { url, user, usersCol } = t.context

  await usersCol.insertOne(user)

  const { data: result } = await axios.get(`${url}/users/${user._id}`)

  t.deepEqual(result, {
    id: user._id,
    name: user.name,
    avatar: user.avatar,
    categories: user.categories,
    stats: { answers: 0, questions: 0, total: null },
    friends: user.friends,
    created: user.created.toISOString(),
    updated: user.updated
  })
})

test('/users/:id PATCH add requested user to friends', async t => {
  const { url, user, usersCol, requestUser, nowSpy } = t.context

  await usersCol.insertOne(user)

  const { data: result } = await axios.patch(
    `${url}/users/${user._id}?add=true`
  )

  t.deepEqual(result, {
    id: requestUser._id,
    name: requestUser.name,
    avatar: requestUser.avatar,
    categories: requestUser.categories,
    friends: [user._id],
    created: requestUser.created.toISOString(),
    updated: requestUser.updated
  })
})

test('/users/:id PATCH add requested category to list of subscriptions', async t => {
  const { url, requestUser } = t.context

  const { data: result } = await axios.patch(`${url}/users?category=history`)

  t.deepEqual(result, {
    id: requestUser._id,
    name: requestUser.name,
    avatar: requestUser.avatar,
    categories: ['history'],
    friends: requestUser.categories,
    created: requestUser.created.toISOString(),
    updated: requestUser.updated
  })
})

test('/users/:id DELETE perform soft delete on user', async t => {
  const { url, requestUser, usersCol, nowSpy } = t.context

  const result = await axios.delete(`${url}/users`)

  const record = await usersCol.findOne({ _id: requestUser._id })
  t.is(result.status, 204)

  t.deepEqual(record, {
    _id: requestUser._id,
    name: 'DELETED',
    email: 'DELETED',
    avatar:
      'https://zcoin.io/wp-content/uploads/2017/01/blank-avatar-300x300.png',
    friends: [],
    categories: [],
    created: requestUser.created,
    updated: nowSpy.returnValues[0]
  })
})
