const test = require('ava')
const uuid = require('uuid/v4')
const sinon = require('sinon')
const axios = require('./helpers/axios')
const server = require('../src/server')
const TransientMap = require('../src/TransientMap')
const constants = require('./helpers/constants')

test.beforeEach(async t => {
  let dbUrl = `mongodb://localhost:27017/test-${uuid()}`
  if (process.env.CI) dbUrl = `mongodb://mongo:27017/test-${uuid()}`

  const usersMap = new TransientMap()
  await usersMap.set(constants.USER_ID, {
    id: constants.USER_ID,
    tokenId: constants.TOKEN_ID,
    isAdmin: false
  })

  const config = {
    dbUrl,
    id: sinon.spy(uuid),
    now: sinon.spy(_ => new Date()),
    usersMap,
    jwtSecret: constants.AUTH_JWT_KEY,
    auth: {
      gClientId: 'a',
      gClientSecret: 'b'
    }
  }

  const { db, httpServer } = await server(config)

  const question = {
    _id: 'ed9760d7-48fb-476d-be4f-91200881b912',
    author: constants.USER_ID,
    header: 'How much costs mans eye?',
    image: 'eye.png',
    description: 'How much costs mans eye on black market?',
    categories: ['people', 'money'],
    status: {
      plus: ['user1'],
      minus: ['user2', 'user3']
    },
    created: new Date(),
    updated: null
  }

  const user = {
    _id: constants.USER_ID,
    name: 'Test user',
    avatar: 'some-picture'
  }

  await db.collection('users').insertOne(user)

  t.context = {
    db,
    questionsCol: db.collection('questions'),
    usersCol: db.collection('users'),
    question,
    user,
    httpServer,
    url: `http://localhost:${httpServer.address().port}`,
    idSpy: config.id,
    nowSpy: config.now
  }
})

test.afterEach(async t => {
  let { db, httpServer } = t.context

  await Promise.all([
    db.dropDatabase(),
    new Promise(resolve => httpServer.close(resolve))
  ])
})

test('/questions/:id/status GET returns status of the question', async t => {
  const { url, question, questionsCol } = t.context

  await questionsCol.insertOne(question)

  const { data: result } = await axios.get(
    `${url}/questions/${question._id}/status`
  )

  t.is(result, -1)
})

test('/questions/:id/up POST votes for the question', async t => {
  const { url, question, questionsCol } = t.context

  await questionsCol.insertOne(question)

  const { data: result } = await axios.post(
    `${url}/questions/${question._id}/up`
  )

  t.is(result, 0)
})

test('/questions/:id/up POST votes versus the question', async t => {
  const { url, question, questionsCol } = t.context

  await questionsCol.insertOne(question)

  const { data: result } = await axios.post(
    `${url}/questions/${question._id}/down`
  )

  t.is(result, -2)
})
