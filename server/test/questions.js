const test = require('ava')
const uuid = require('uuid/v4')
const sinon = require('sinon')
const axios = require('./helpers/axios')
const server = require('../src/server')
const TransientMap = require('../src/TransientMap')
const constants = require('./helpers/constants')

test.beforeEach(async t => {
  let dbUrl = `mongodb://localhost:27017/test-${uuid()}`
  if (process.env.CI) dbUrl = `mongodb://mongo:27017/test-${uuid()}`

  const usersMap = new TransientMap()
  await usersMap.set(constants.USER_ID, {
    id: constants.USER_ID,
    tokenId: constants.TOKEN_ID,
    isAdmin: false
  })

  const config = {
    dbUrl,
    id: sinon.spy(uuid),
    now: sinon.spy(_ => new Date()),
    usersMap,
    jwtSecret: constants.AUTH_JWT_KEY,
    auth: {
      gClientId: 'a',
      gClientSecret: 'b'
    }
  }

  const { db, httpServer } = await server(config)

  const question = {
    _id: 'ed9760d7-48fb-476d-be4f-91200881b912',
    author: constants.USER_ID,
    header: 'How much costs mans eye?',
    image: 'eye.png',
    description: 'How much costs mans eye on black market?',
    categories: ['people', 'money'],
    status: {
      plus: [],
      minus: []
    },
    created: new Date(),
    updated: null
  }

  const user = {
    _id: constants.USER_ID,
    name: 'Test user',
    avatar: 'some-picture'
  }

  await db.collection('users').insertOne(user)

  t.context = {
    db,
    questionsCol: db.collection('questions'),
    usersCol: db.collection('users'),
    question,
    user,
    httpServer,
    url: `http://localhost:${httpServer.address().port}`,
    idSpy: config.id,
    nowSpy: config.now
  }
})

test.afterEach(async t => {
  let { db, httpServer } = t.context

  await Promise.all([
    db.dropDatabase(),
    new Promise(resolve => httpServer.close(resolve))
  ])
})

test('/questions/ GET return list of all questions', async t => {
  const { url, questionsCol, question } = t.context

  const limit = Math.floor(Math.random() * 10) + 1

  for (let i = 0; i < limit; i++) {
    await questionsCol.insertOne({
      ...question,
      _id: uuid()
    })
  }
  const result = await axios.get(`${url}/questions?limit=${limit}&page=0`)

  t.is(result.data.length, limit)

  t.is(
    await questionsCol.countDocuments(),
    parseInt(result.headers.ozo_pagination_total)
  )
})

test('/questions/ GET return list of questions for category', async t => {
  const { url, questionsCol, question } = t.context

  const categories = [
    'history',
    'political',
    'history',
    'political',
    'science',
    'history'
  ]

  for (let i = 0; i < categories.length; i++) {
    await questionsCol.insertOne({
      ...question,
      _id: uuid(),
      categories: [categories[i]]
    })
  }
  const result = await axios.get(
    `${url}/questions?limit=10&page=0&category=history`
  )

  t.is(result.data.length, 3)

  t.is(
    await questionsCol.countDocuments({ categories: 'history' }),
    parseInt(result.headers.ozo_pagination_total)
  )
})

test('/questions/ GET return list of questions of author', async t => {
  const { url, questionsCol, question, usersCol } = t.context

  const authors = [
    {
      _id: '39ad6cb8-2dc2-44d0-bce9-7ab078386396',
      name: 'Test user1',
      avatar: 'some-picture'
    },
    {
      _id: '626f5a65-113b-4e69-b98b-20a048221538',
      name: 'Test user2',
      avatar: 'some-picture'
    }
  ]
  await usersCol.insertOne(authors[0])
  await usersCol.insertOne(authors[1])

  await questionsCol.insertOne({
    ...question,
    _id: uuid(),
    author: authors[0]._id
  })

  await questionsCol.insertOne({
    ...question,
    _id: uuid(),
    author: authors[1]._id
  })

  await questionsCol.insertOne({
    ...question,
    _id: uuid(),
    author: authors[1]._id
  })

  const result = await axios.get(
    `${url}/questions?limit=10&page=0&author=626f5a65-113b-4e69-b98b-20a048221538`
  )

  t.is(result.data.length, 2)

  t.is(
    await questionsCol.countDocuments({
      author: '626f5a65-113b-4e69-b98b-20a048221538'
    }),
    parseInt(result.headers.ozo_pagination_total)
  )
})

test('/questions/:id GET return question', async t => {
  const { url, questionsCol, question, user } = t.context

  await questionsCol.insertOne(question)

  const { data: result } = await axios.get(`${url}/questions/${question._id}`)

  t.deepEqual(result, {
    id: question._id,
    author: {
      id: user._id,
      name: user.name,
      avatar: user.avatar
    },
    categories: question.categories,
    description: question.description,
    header: question.header,
    image: question.image,
    status: 0,
    created: question.created.toISOString(),
    updated: question.updated
  })
})

test('/questions/ POST creates new questions and returns its value', async t => {
  const { url, questionsCol, idSpy } = t.context

  const question = {
    header: 'How much costs mans eye?',
    image: 'eye.png',
    description: 'How much costs mans eye on black market?',
    categories: ['people', 'money']
  }

  const { data: result } = await axios.post(`${url}/questions`, question)

  const record = await questionsCol.findOne({ _id: idSpy.returnValues[0] })

  t.deepEqual(question, {
    header: record.header,
    image: record.image,
    description: record.description,
    categories: record.categories
  })

  t.deepEqual(result, {
    id: record._id,
    author: constants.USER_ID,
    header: record.header,
    image: record.image,
    description: record.description,
    categories: record.categories,
    status: 0,
    created: record.created.toISOString(),
    updated: record.updated
  })
})

test('/questions POST with invalid inputs return 422 error', async t => {
  const { url } = t.context

  const question = {
    header: 899,
    image: 'eye.png',
    description: 'How much costs mans eye on black market?',
    categories: ['people', 'money']
  }

  const { response: error } = await t.throwsAsync(_ =>
    axios.post(`${url}/questions`, question)
  )

  t.is(error.status, 422)
})

test('/questions/:id PATCH updates question and return new value', async t => {
  const { url, questionsCol, question, nowSpy } = t.context

  await questionsCol.insertOne(question)

  const update = { header: 'My new header!' }

  const { data: result } = await axios.patch(
    `${url}/questions/${question._id}`,
    update
  )

  t.deepEqual(result, {
    id: question._id,
    author: question.author,
    header: update.header,
    description: question.description,
    categories: question.categories,
    image: question.image,
    status: 0,
    created: question.created.toISOString(),
    updated: nowSpy.returnValues[0].toISOString()
  })
})

test('/questions/:id PATCH with invalid inputs returns 422 error', async t => {
  const { url, questionsCol, question, nowSpy } = t.context

  await questionsCol.insertOne(question)

  const update = { header: 45966 }

  const { response: error } = await t.throwsAsync(_ =>
    axios.patch(`${url}/questions/${question._id}`, update)
  )
  t.is(error.status, 422)
})

test('/questions/:id DELETE removes question', async t => {
  const { url, questionsCol, question, nowSpy } = t.context

  await questionsCol.insertOne({...question, image: ''})

  const result = await axios.delete(`${url}/questions/${question._id}`)

  t.is(result.status, 204)
})

test('/questions/:id DELETE with non existing id return 404', async t => {
  const { url } = t.context

  const { response: error } = await t.throwsAsync(_ =>
    axios.delete(`${url}/questions/${uuid()}`)
  )

  t.is(error.status, 404)
})
