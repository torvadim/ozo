const test = require('ava')
const uuid = require('uuid/v4')
const sinon = require('sinon')
const axios = require('./helpers/axios')
const server = require('../src/server')
const TransientMap = require('../src/TransientMap')
const constants = require('./helpers/constants')

test.beforeEach(async t => {
  let dbUrl = `mongodb://localhost:27017/test-${uuid()}`
  if (process.env.CI) dbUrl = `mongodb://mongo:27017/test-${uuid()}`

  const usersMap = new TransientMap()
  await usersMap.set(constants.USER_ID, {
    id: constants.USER_ID,
    tokenId: constants.TOKEN_ID,
    isAdmin: false
  })

  const config = {
    dbUrl,
    id: sinon.spy(uuid),
    now: sinon.spy(_ => new Date()),
    usersMap,
    jwtSecret: constants.AUTH_JWT_KEY,
    auth: {
      gClientId: 'a',
      gClientSecret: 'b'
    }
  }

  const { db, httpServer } = await server(config)

  const answer = {
    _id: 'f4092070-1250-4711-a24f-bed86f3a0715',
    questionId: 'ed9760d7-48fb-476d-be4f-91200881b912',
    text: 'This is my answer',
    author: constants.USER_ID,
    status: {
      plus: ['user1', 'user3'],
      minus: ['user2']
    },
    created: new Date(),
    updated: null
  }

  const question = {
    _id: 'ed9760d7-48fb-476d-be4f-91200881b912',
    author: '522fa07d-7eac-4a3a-b979-b6c7dc144675',
    header: 'How much costs mans eye?',
    image: 'eye.png',
    description: 'How much costs mans eye on black market?',
    status: {
      plus: [],
      minus: []
    },
    categories: ['people', 'money'],
    created: new Date(),
    updated: null
  }

  const user = {
    _id: constants.USER_ID,
    name: 'Test user',
    avatar: 'some-picture'
  }

  const secondUser = {
    _id: '522fa07d-7eac-4a3a-b979-b6c7dc144675',
    name: 'Test user',
    avatar: 'some-picture'
  }

  const questionCol = db.collection('questions')
  const usersCol = db.collection('users')

  await usersCol.insertOne(user)
  await usersCol.insertOne(secondUser)

  await questionCol.insertOne(question)

  t.context = {
    db,
    answersCol: db.collection('answers'),
    question,
    answer,
    httpServer,
    url: `http://localhost:${httpServer.address().port}`,
    idSpy: config.id,
    nowSpy: config.now
  }
})

test.afterEach(async t => {
  let { db, httpServer } = t.context

  await Promise.all([
    db.dropDatabase(),
    new Promise(resolve => httpServer.close(resolve))
  ])
})

test('/answer/:id/status GET returns status of the answer', async t => {
  const { url, question, answer, answersCol } = t.context

  await answersCol.insertOne(answer)

  const { data: result } = await axios.get(
    `${url}/questions/${question._id}/answers/${answer._id}/status`
  )

  t.is(result, 1)
})

test('/answer/:id/up POST votes for the answer', async t => {
  const { url, question, answer, answersCol } = t.context

  await answersCol.insertOne(answer)

  const { data: result } = await axios.post(
    `${url}/questions/${question._id}/answers/${answer._id}/up`
  )

  t.is(result, 2)
})

test('/answer/:id/up POST votes versus the answer', async t => {
  const { url, question, answer, answersCol } = t.context

  await answersCol.insertOne(answer)

  const { data: result } = await axios.post(
    `${url}/questions/${question._id}/answers/${answer._id}/down`
  )

  t.is(result, 0)
})
