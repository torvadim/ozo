function koaValidation({ joi = require('@hapi/joi'), joiOptions } = {}) {
  return (ctx, schema) => {
    const input = Object.keys(schema).reduce(
      (input, key) => ({
        ...input,
        [key]: ctx.request[key] === undefined ? ctx[key] : ctx.request[key]
      }),
      {}
    )

    const { value, error } = joi.validate(input, schema, joiOptions)

    if (error) throw error
    return value
  }
}

koaValidation.validate = koaValidation()

module.exports = koaValidation
