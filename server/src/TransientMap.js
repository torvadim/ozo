class TransientMap {
  constructor(ttl = 60 * 1000 * 15, interval = 1000) {
    this.map = new Map()
    this.ttl = ttl
    this.checkLoop = setInterval(this.checkExpiration.bind(this), interval)
  }

  get(key) {
    let item = this.map.get(key)
    if (!item) return Promise.resolve(undefined)

    const now = Date.now()
    const { ttl } = this

    if (now >= item._createdAt + ttl) {
      return Promise.resolve(undefined)
    }

    item = { ...item }
    delete item._createdAt

    return Promise.resolve(item)
  }

  set(key, value) {
    value._createdAt = Date.now()
    return Promise.resolve(this.map.set(key, value))
  }

  delete(key) {
    return Promise.resolve(this.map.delete(key))
  }

  checkExpiration() {
    const now = Date.now()

    const { ttl } = this

    this.map.forEach((value, key, map) => {
      if (now >= value._createdAt + ttl) {
        map.delete(key)
      }
    })
  }
}

module.exports = TransientMap
