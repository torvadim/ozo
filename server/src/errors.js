const { STATUS_CODES } = require('http')

const logger = require('pino')()

const errCodes = {
  401: 'UNAUTHORIZED',
  403: 'FORBIDDEN',
  404: 'NOT_FOUND',
  422: 'VALIDATION_FAILED',
  500: 'INTERNAL_SERVER_ERROR'
}

module.exports = () => {
  return async (ctx, next) => {
    try {
      await next()
    } catch (err) {
      // validation errors
      if (err.isJoi) {
        ctx.status = 422
        ctx.body = {
          code: errCodes[422],
          message: 'Validation failed',
          errors: err.details.map(({ path: [, ...path], message }) => ({
            field: path.join('.'),
            message: message.replace(/"/g, "'") // make message more json friendly
          })),
          messageParams: err.messageParams
        }

        logger.warn(`Validation error: ${ctx.method} ${ctx.url}`, { err })

        return
      }

      // client errors
      if (err.statusCode < 500) {
        ctx.status = err.statusCode
        ctx.body = {
          code: err.code || errCodes[ctx.status],
          message: STATUS_CODES[ctx.status],
          messageParams: err.messageParams
        }

        logger.warn(`Client error: ${ctx.method} ${ctx.url}`, { err })

        return
      }

      // server errors
      ctx.status = err.statusCode || 500
      ctx.body = {
        code: err.code || errCodes[ctx.status],
        message:
          process.env.NODE_ENV === 'production' ? 'Unhandled Error' : err.stack
      }

      logger.error(`Unhandled error: ${ctx.method} ${ctx.url}`, { err })
    }
  }
}
