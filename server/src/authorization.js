const jwt = require('jsonwebtoken')

function authorization({ db, jwtSecret, usersMap }) {
  return async (ctx, next) => {
    const [type, creds] = ctx.get('authorization').split(' ')

    const payload = type === 'Bearer' ? decode(creds, jwtSecret) : null
    ctx.assert(payload, 401)

    const { userId, tokenId, isAdmin } = payload
    ctx.assert(userId && tokenId && isAdmin != null, 401)

    let user = await usersMap.get(userId)

    if (!user || user.tokenId !== tokenId) {
      user = await db.collection('users').findOne({ _id: userId })
    }

    if (!user || user.tokenId !== tokenId) {
      await usersMap.delete(userId)
      ctx.throw(401)
    }

    await usersMap.set(userId, user)

    ctx.state.user = { userId, tokenId, isAdmin: user.isAdmin }

    return next()
  }
}

function decode(token, secret) {
  try {
    return jwt.verify(token, secret)
  } catch (err) {
    return null
  }
}

module.exports = authorization
