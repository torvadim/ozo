const uuid = require('uuid/v4')
const Router = require('koa-router')
const joi = require('@hapi/joi')

const { validate } = require('../koaValidation')

module.exports = function answers({
  db,
  authorize,
  id = uuid(),
  now = new Date()
}) {
  const questionsCol = db.collection('questions')
  const answersCol = db.collection('answers')
  const usersCol = db.collection('users')

  return new Router({ prefix: '/questions/:questionId/answers' })
    .get('/', list)
    .use(async (ctx, next) => {
      if (ctx.method === 'GET') return next()
      await authorize(ctx, next)
    })
    .post('/', create)
    .param('id', async (id, ctx, next) => {
      const { error } = joi.validate(
        id,
        joi
          .string()
          .guid()
          .required()
      )

      if (error) throw error

      if (ctx.state.user.isAdmin) return next()

      const answer = await answersCol.findOne({ _id: id })
      ctx.assert(ctx.state.user.userId === answer.author, 401)
      await authorize(ctx, next)
    })
    .patch('/:id', update)
    .delete('/:id', remove)
    .routes()

  async function list(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const answers = await answersCol
      .aggregate([
        {
          $addFields: {
            status_count: {
              $subtract: [
                { $size: { $ifNull: ['$status.plus', []] } },
                { $size: { $ifNull: ['$status.minus', []] } }
              ]
            }
          }
        },
        {
          $match: { questionId: params.questionId }
        },
        {
          $sort: { status_count: -1 }
        }
      ])
      .toArray()

    await asyncForEach(answers, async answer => {
      let user = await usersCol.findOne({ _id: answer.author })

      if (user) {
        answer.author = {
          id: user._id,
          avatar: user.avatar,
          name: user.name
        }
      }
    })

    ctx.assert(answers, 404)

    ctx.body = answers.map(map)
  }

  async function create(ctx) {
    const { params, body } = validate(ctx, {
      params: joi
        .object({
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required(),
      body: joi
        .object({
          text: joi
            .string()
            .max(1000)
            .required()
        })
        .required()
    })

    const { text } = body

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const answer = {
      _id: id(),
      questionId: params.questionId,
      author: ctx.state.user.userId,
      text,
      status: { plus: [], minus: [] },
      created: now(),
      updated: null
    }

    await answersCol.insertOne(answer)

    ctx.body = map(answer)
  }

  async function update(ctx) {
    const { params, body } = validate(ctx, {
      params: joi
        .object({
          id: joi
            .string()
            .uuid()
            .required(),
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required(),
      body: joi
        .object({
          text: joi
            .string()
            .max(1000)
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const { value: answer } = await answersCol.findOneAndUpdate(
      { _id: params.id },
      { $set: { text: body.text, updated: now() } },
      { returnOriginal: false }
    )

    ctx.assert(answer, 404)

    ctx.body = map(answer)
  }

  async function remove(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          id: joi
            .string()
            .uuid()
            .required(),
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const { value } = await answersCol.findOneAndDelete({ _id: params.id })
    ctx.assert(value, 404)

    ctx.status = 204
    ctx.body = {}
  }

  async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }

  function map(answer) {
    return {
      id: answer._id,
      author: answer.author,
      text: answer.text,
      status: answer.status.plus.length - answer.status.minus.length,
      created: answer.created,
      updated: answer.updated
    }
  }
}
