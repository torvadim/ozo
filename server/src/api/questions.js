const uuid = require('uuid/v4')
const Router = require('koa-router')
const joi = require('@hapi/joi')
const mongodb = require('mongodb')

const { validate } = require('../koaValidation')

module.exports = function questions({
  db,
  authorize,
  id = uuid,
  now = _ => new Date()
}) {
  const questionsCol = db.collection('questions')
  const usersCol = db.collection('users')
  const answersCol = db.collection('answers')
  const bucket = new mongodb.GridFSBucket(db)

  const questionRoutes = new Router({ prefix: '/questions' })
    .get('/:questionId', get)
    .get('/', listAll)
    .use(async (ctx, next) => {
      await authorize(ctx, next)
    })
    .post('/', create)
    .param('id', async (id, ctx, next) => {
      const { error } = joi.validate(
        id,
        joi
          .string()
          .guid()
          .required()
      )

      if (error) throw error

      if (!ctx.state.user.isAdmin) {
        const question = await questionsCol.findOne({ _id: id })
        ctx.assert(question, 404)
        ctx.assert(ctx.state.user.userId === question.author, 401)
      }

      return next()
    })
    .patch('/:id', update)
    .delete('/:id', remove)
    .routes()

  const feedRoutes = new Router({ prefix: '/feed' })
    .use(async (ctx, next) => {
      await authorize(ctx, next)
    })
    .get('/', feed)
    .routes()

  return new Router()
    .use(feedRoutes)
    .use(questionRoutes)
    .routes()

  async function get(ctx) {
    const { params, query } = validate(ctx, {
      params: joi
        .object({
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })

    ctx.assert(question, 404)

    const user = await usersCol.findOne({ _id: question.author })

    ctx.assert(user, 404)

    question.author = {
      id: user._id,
      name: user.name,
      avatar: user.avatar
    }

    ctx.body = map(question)
  }

  async function listAll(ctx) {
    const { query } = validate(ctx, {
      query: joi
        .object({
          page: joi
            .number()
            .integer()
            .min(0)
            .default(0),
          limit: joi
            .number()
            .integer()
            .min(0)
            .max(50)
            .default(20),
          category: joi.string().max(50),
          author: joi.string().max(50),
          byStatus: joi.boolean()
        })
        .optional()
    })

    const findQuery = {}
    const sortQuery = {}

    if (query.category) findQuery.categories = query.category
    if (query.author) findQuery.author = query.author
    if (query.byStatus) sortQuery.status_count = -1
    else sortQuery.created = -1

    const questions = await questionsCol
      .aggregate([
        {
          $addFields: {
            status_count: {
              $subtract: [
                { $size: { $ifNull: ['$status.plus', []] } },
                { $size: { $ifNull: ['$status.minus', []] } }
              ]
            }
          }
        },
        {
          $match: findQuery
        },
        {
          $sort: sortQuery
        }
      ])
      .skip(query.page > 0 ? (query.page - 1) * query.limit : 0)
      .limit(query.limit)
      .toArray()

    ctx.assert(questions, 404)

    await asyncForEach(questions, async question => {
      let user = await usersCol.findOne({ _id: question.author })

      if (user) {
        question.author = {
          id: user._id,
          avatar: user.avatar,
          name: user.name
        }
      }
    })

    ctx.set(
      'OZO_Pagination_Total',
      await questionsCol.countDocuments(findQuery)
    )

    ctx.body = questions.map(map)
  }

  async function feed(ctx) {
    const { query } = validate(ctx, {
      query: joi
        .object({
          page: joi
            .number()
            .integer()
            .min(0)
            .default(0),
          limit: joi
            .number()
            .integer()
            .min(0)
            .max(50)
            .default(20),
          byStatus: joi.boolean()
        })
        .optional()
    })

    ctx.assert(ctx.state.user.userId, 403)

    const user = await usersCol.findOne({ _id: ctx.state.user.userId })

    const findQuery = { $or: [] }
    const sortQuery = {}

    if (user.categories.length)
      findQuery.$or.push({ categories: { $in: user.categories } })
    if (user.friends.length)
      findQuery.$or.push({ author: { $in: user.friends } })
    if (query.byStatus) sortQuery.status_count = -1
    else sortQuery.created = -1

    const questions = await questionsCol
      .aggregate([
        {
          $addFields: {
            status_count: {
              $subtract: [
                { $size: { $ifNull: ['$status.plus', []] } },
                { $size: { $ifNull: ['$status.minus', []] } }
              ]
            }
          }
        },
        {
          $match: findQuery
        },
        {
          $sort: sortQuery
        }
      ])
      .skip(query.page > 0 ? (query.page - 1) * query.limit : 0)
      .limit(query.limit)
      .toArray()

    ctx.assert(questions, 404)

    await asyncForEach(questions, async question => {
      let user = await usersCol.findOne({ _id: question.author })

      if (user) {
        question.author = {
          id: user._id,
          avatar: user.avatar,
          name: user.name
        }
      }
    })

    ctx.set(
      'OZO_Pagination_Total',
      await questionsCol.countDocuments(findQuery)
    )

    ctx.body = questions.map(map)
  }

  async function create(ctx) {
    const { body } = validate(ctx, {
      body: joi.object({
        header: joi
          .string()
          .max(120)
          .required(),
        image: joi
          .string()
          .max(1000)
          .allow(''),
        categories: joi
          .array()
          .items(joi.string().max(50))
          .allow('')
          .required(),
        description: joi
          .string()
          .max(1000)
          .required()
      })
    })

    ctx.assert(ctx.state.user.userId, 403)

    const question = {
      _id: id(),
      ...body,
      author: ctx.state.user.userId,
      status: {
        plus: [],
        minus: []
      },
      created: now(),
      updated: null
    }

    await questionsCol.insertOne(question)

    ctx.body = map(question)
  }

  async function update(ctx) {
    const { params, body } = validate(ctx, {
      params: joi.object({
        id: joi
          .string()
          .uuid()
          .required()
      }),
      body: joi.object({
        header: joi.string().max(120),
        image: joi
          .string()
          .max(100)
          .allow(''),
        categories: joi.array().items(joi.string().max(50)),
        description: joi.string().max(1000)
      })
    })

    const fieldsToUpdate = Object.entries(body).reduce(
      (acc, [key, val]) => {
        acc[key] = val
        return acc
      },
      {
        ['updated']: now()
      }
    )

    const { value: question } = await questionsCol.findOneAndUpdate(
      { _id: params.id },
      { $set: fieldsToUpdate },
      { returnOriginal: false }
    )

    ctx.assert(question, 404)

    ctx.body = map(question)
  }

  async function remove(ctx) {
    const { params } = validate(ctx, {
      params: joi.object({
        id: joi
          .string()
          .uuid()
          .required()
      })
    })

    const question = await questionsCol.findOne({ _id: params.id })

    question.image && bucket.delete(question.image)

    const { value } = await questionsCol.findOneAndDelete({
      _id: params.id
    })

    await answersCol.deleteMany({ questionId: params.id })

    ctx.assert(value, 404)

    ctx.status = 204
    ctx.body = {}
  }

  async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }

  function map(question) {
    return {
      id: question._id,
      author: question.author,
      header: question.header,
      description: question.description,
      image: question.image,
      status: question.status.plus.length - question.status.minus.length,
      comments: question.comments,
      categories: question.categories,
      created: question.created,
      updated: question.updated
    }
  }
}
