const Router = require('koa-router')
const mongodb = require('mongodb')
const joi = require('@hapi/joi')
const logger = require('pino')()

const fs = require('fs')

const { validate } = require('../koaValidation')

function images({ db, id, authorize }) {
  const bucket = new mongodb.GridFSBucket(db)

  return new Router({ prefix: '/img' })
    .get('/:id', get)
    .use(async (ctx, next) => {
      await authorize(ctx, next)
    })
    .post('/', upload)
    .routes()

  async function get(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          id: joi.string().required()
        })
        .required()
    })

    ctx.headers = { 'Content-Type': 'image/jpeg' }
    ctx.body = bucket.openDownloadStream(`${params.id}`)
  }

  async function upload(ctx) {
    const imageId = id()

    const file = ctx.request.files.image

    ctx.assert(file, 400)

    const reader = fs.createReadStream(file.path)

    const uploadStream = bucket.openUploadStreamWithId(imageId, file.name)

    reader.pipe(uploadStream)

    uploadStream.on('finish', () => {
      ctx.status = 200
    })

    uploadStream.on('error', err => {
      ctx.status = 500
      logger.error('Upload failed', err)
      ctx.throw(500, 'Upload failed')
    })

    ctx.body = `${imageId}`
  }
}

module.exports = images
