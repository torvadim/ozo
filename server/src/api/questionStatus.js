const uuid = require('uuid/v4')
const Router = require('koa-router')
const joi = require('@hapi/joi')
const { validate } = require('../koaValidation')

module.exports = function questionStatus({
  db,
  id = uuid,
  authorize,
  now = _ => new Date()
}) {
  const questionsCol = db.collection('questions')

  return new Router({ prefix: '/questions/:questionId' })
    .get('/status', get)
    .use(async (ctx, next) => {
      await authorize(ctx, next)
    })
    .post('/up', voteUp)
    .post('/down', voteDown)
    .routes()

  async function get(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({
      _id: params.questionId
    })
    ctx.assert(question, 404)

    ctx.body = question.status.plus.length - question.status.minus.length
  }

  async function voteUp(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    ctx.assert(ctx.state.user.userId, 403)

    const { value: question } = await questionsCol.findOneAndUpdate(
      {
        _id: params.questionId
      },
      {
        $addToSet: {
          'status.plus': ctx.state.user.userId
        },
        $pull: {
          'status.minus': ctx.state.user.userId
        }
      },
      { returnOriginal: false }
    )
    ctx.assert(question, 404)

    ctx.body = question.status.plus.length - question.status.minus.length
  }

  async function voteDown(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    ctx.assert(ctx.state.user.userId, 403)

    const { value: question } = await questionsCol.findOneAndUpdate(
      {
        _id: params.questionId
      },
      {
        $addToSet: {
          'status.minus': ctx.state.user.userId
        },
        $pull: {
          'status.plus': ctx.state.user.userId
        }
      },
      { returnOriginal: false }
    )
    ctx.assert(question, 404)

    ctx.body = question.status.plus.length - question.status.minus.length
  }
}
