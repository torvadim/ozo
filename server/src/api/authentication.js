const querystring = require('querystring')
const axios = require('axios')
const joi = require('@hapi/joi')
const jwt = require('jsonwebtoken')
const Router = require('koa-router')
const logger = require('pino')()

const { validate } = require('../koaValidation')

function authentication({ db, usersMap, auth, id, now }) {
  const col = db.collection('users')
  const { gClientId, gClientSecret, fClientId, fClientSecret, jwtSecret } = auth

  return new Router({ prefix: '/auth' })
    .get('/:provider/redirect', redirect)
    .get('/:provider/callback', callback)
    .routes()

  async function redirect(ctx) {
    const { query, params } = validate(ctx, {
      query: joi
        .object({
          returnUrl: joi
            .string()
            .uri()
            .required()
        })
        .required(),
      params: joi
        .object({
          provider: joi
            .string()
            .valid(['google', 'facebook'])
            .required()
        })
        .required()
    })

    const { returnUrl } = query
    const { provider } = params

    if (provider === 'google') {
      const queryStr = querystring.encode({
        client_id: gClientId,
        redirect_uri: `${ctx.origin}/auth/${provider}/callback`,
        response_type: 'code',
        scope:
          'https://www.googleapis.com/auth/userinfo.email ' +
          'https://www.googleapis.com/auth/userinfo.profile',
        prompt: 'select_account',
        state: querystring.encode({ returnUrl })
      })

      ctx.redirect(`https://accounts.google.com/o/oauth2/v2/auth?${queryStr}`)
    } else if (provider === 'facebook') {
      const queryStr = querystring.encode({
        client_id: fClientId,
        redirect_uri: `${ctx.origin}/auth/${provider}/callback`,
        scope: 'email',
        auth_type: 'rerequest',
        state: querystring.encode({ returnUrl })
      })

      ctx.redirect(`https://www.facebook.com/v4.0/dialog/oauth?${queryStr}`)
    }
  }

  async function callback(ctx) {
    const { query, params } = validate(ctx, {
      query: joi
        .object()
        .required()
        .unknown(true)
        .keys({
          code: joi.string().required(),
          scope: joi.string(),
          state: joi.string().required()
        }),
      params: joi
        .object({
          provider: joi
            .string()
            .valid(['google', 'facebook'])
            .required()
        })
        .required()
    })

    const { provider } = params

    const { code, state } = query

    const { returnUrl } = querystring.decode(state)

    if (provider === 'google') {
      // obtain access token
      const {
        data: { access_token: accessToken }
      } = await axios.post(
        'https://www.googleapis.com/oauth2/v4/token',
        querystring.encode({
          code,
          client_id: gClientId,
          client_secret: gClientSecret,
          redirect_uri: `${ctx.origin}/auth/${provider}/callback`,
          grant_type: 'authorization_code'
        }),
        { headers: { 'content-Type': 'application/x-www-form-urlencoded' } }
      )

      ctx.assert(accessToken, 500, 'Could not obtain access token from Google.')
      logger.info('access token obtained', { accessToken })

      // fetch user profile data
      const { data: userProfile } = await axios.get(
        `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${accessToken}`
      )

      ctx.assert(userProfile, 500, 'Could not fetch user profile from Google.')
      logger.info('user profile received', { userProfile })

      // generate new tokenId
      const tokenId = id()

      if (userProfile.picture && !userProfile.name) {
        userProfile.picture = `https://identicon-api.herokuapp.com/${userProfile.name}/400?format=png`
      }

      // check if user exists and save profile data and tokenId
      let { value: user } = await col.findOneAndUpdate(
        { email: userProfile.email },
        {
          $set: {
            name: userProfile.name,
            providers: { googleId: userProfile.id },
            avatar: userProfile.picture,
            tokenId
          }
        }
      )

      //registration process
      if (!user) {
        user = {
          _id: id(),
          email: userProfile.email,
          name: userProfile.name,
          providers: { googleId: userProfile.id },
          avatar: userProfile.picture,
          isAdmin: false,
          friends: [],
          categories: [],
          created: now(),
          isAdmin: false,
          tokenId
        }
        await col.insertOne(user)
      }

      // create jwt token
      const token = await jwt.sign(
        { tokenId, userId: user._id, isAdmin: user.isAdmin },
        jwtSecret,
        { expiresIn: '10d' }
      )
      logger.info('token created', { token })

      // redirect to return url

      await usersMap.set(user._id, user)

      ctx.redirect(`${returnUrl}?token=${token}`)
    } else if (provider === 'facebook') {
      // obtain access token
      const queryStr = querystring.encode({
        client_id: fClientId,
        redirect_uri: `${ctx.origin}/auth/${provider}/callback`,
        client_secret: fClientSecret,
        code: code
      })
      const {
        data: { access_token: accessToken }
      } = await axios.get(
        `https://graph.facebook.com/v4.0/oauth/access_token?${queryStr}`
      )

      ctx.assert(accessToken, 500, 'Could not obtain access token from Google.')
      logger.info('access token obtained', { accessToken })

      // obtain user profile

      const queryStringUser = querystring.encode({
        fields: 'id,name email picture',
        access_token: accessToken
      })

      const { data: userProfile } = await axios.get(
        `https://graph.facebook.com/me?fields=id,name,email,picture.width(800).height(800)&access_token=${accessToken}`
      )

      ctx.assert(userProfile, 500, 'Could not fetch user profile from Google.')
      logger.info('user profile received', { userProfile })

      // generate new tokenId
      const tokenId = id()

      if (userProfile.picture.data.url && !userProfile.name) {
        userProfile.picture.data.url = `https://identicon-api.herokuapp.com/${userProfile.name}/400?format=png`
      }

      // check if user exists and save profile data and tokenId
      let { value: user } = await col.findOneAndUpdate(
        { email: userProfile.email },
        {
          $set: {
            name: userProfile.name,
            providers: { facebookId: userProfile.id },
            avatar: userProfile.picture.data.url,
            tokenId
          }
        }
      )

      //registration process
      if (!user) {
        user = {
          _id: id(),
          email: userProfile.email,
          name: userProfile.name,
          providers: { facebookId: userProfile.id },
          avatar: userProfile.picture.data.url,
          isAdmin: false,
          friends: [],
          categories: [],
          created: now(),
          isAdmin: false,
          tokenId
        }
        await col.insertOne(user)
      }

      // create jwt token
      const token = await jwt.sign(
        { tokenId, userId: user._id, isAdmin: user.isAdmin },
        jwtSecret,
        { expiresIn: '10d' }
      )
      logger.info('token created', { token })

      await usersMap.set(user._id, user)

      // redirect to return url
      ctx.redirect(`${returnUrl}?token=${token}`)
    }
  }
}

module.exports = authentication
