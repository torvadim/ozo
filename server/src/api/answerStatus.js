const uuid = require('uuid/v4')
const Router = require('koa-router')
const joi = require('@hapi/joi')
const { validate } = require('../koaValidation')

//TODO Refactor response body

module.exports = function answerStatus({
  db,
  authorize,
  id = uuid,
  now = _ => new Date()
}) {
  const questionsCol = db.collection('questions')
  const answerCol = db.collection('answers')

  return new Router({
    prefix: '/questions/:questionId/answers/:answerId'
  })
    .get('/status', get)
    .use(async (ctx, next) => {
      await authorize(ctx, next)
    })
    .post('/up', voteUp)
    .post('/down', voteDown)
    .routes()

  async function get(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          answerId: joi
            .string()
            .uuid()
            .required(),
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const answer = await answerCol.findOne({
      _id: params.answerId
    })
    ctx.assert(answer, 404)

    ctx.body = answer.status.plus.length - answer.status.minus.length
  }

  async function voteUp(ctx) {
    const { params } = validate(ctx, {
      params: joi
        .object({
          answerId: joi
            .string()
            .uuid()
            .required(),
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const { value: answer } = await answerCol.findOneAndUpdate(
      {
        _id: params.answerId
      },
      {
        $addToSet: {
          'status.plus': ctx.state.user.userId
        },
        $pull: {
          'status.minus': ctx.state.user.userId
        }
      },
      { returnOriginal: false }
    )
    ctx.assert(answer, 404)

    ctx.body = answer.status.plus.length - answer.status.minus.length
  }

  async function voteDown(ctx) {
    const { params, body } = validate(ctx, {
      params: joi
        .object({
          answerId: joi
            .string()
            .uuid()
            .required(),
          questionId: joi
            .string()
            .uuid()
            .required()
        })
        .required()
    })

    const question = await questionsCol.findOne({ _id: params.questionId })
    ctx.assert(question, 404)

    const { value: answer } = await answerCol.findOneAndUpdate(
      {
        _id: params.answerId
      },
      {
        $addToSet: {
          'status.minus': ctx.state.user.userId
        },
        $pull: {
          'status.plus': ctx.state.user.userId
        }
      },
      { returnOriginal: false }
    )
    ctx.assert(answer, 404)

    ctx.body = answer.status.plus.length - answer.status.minus.length
  }
}
