const Router = require('koa-router')
const joi = require('@hapi/joi')

const { validate } = require('../koaValidation')

module.exports = function users({ db, id, authorize, now }) {
  const usersCol = db.collection('users')
  const questionsCol = db.collection('questions')
  const answersCol = db.collection('answers')

  return new Router({ prefix: '/users' })
    .get('/:id', get)
    .use(async (ctx, next) => {
      await authorize(ctx, next)
    })
    .patch('/', subscribeToCat)
    .patch('/:id', addToFriends)
    .delete('/', remove)
    .routes()

  async function get(ctx) {
    const { params } = validate(ctx, {
      params: joi.object({
        id: joi
          .string()
          .max(50)
          .required()
      })
    })

    let user = await usersCol.findOne({ _id: params.id })

    let questions = await questionsCol
      .aggregate([
        {
          $match: { author: params.id }
        },
        {
          $addFields: {
            status_count: {
              $subtract: [
                { $size: { $ifNull: ['$status.plus', []] } },
                { $size: { $ifNull: ['$status.minus', []] } }
              ]
            }
          }
        },
        { $project: { status: '$status_count' } }
      ])
      .toArray()

    let answers = await answersCol
      .aggregate([
        {
          $match: { author: params.id }
        },
        {
          $addFields: {
            status_count: {
              $subtract: [
                { $size: { $ifNull: ['$status.plus', []] } },
                { $size: { $ifNull: ['$status.minus', []] } }
              ]
            }
          }
        },
        { $project: { status: '$status_count' } }
      ])
      .toArray()

    let stats = { questions: questions.length, answers: answers.length }

    let questionsStatus, answersStatus

    if (questions.length)
      questionsStatus = questions
        .map(item => item.status)
        .reduce((prev, next) => prev + next)
    if (answers.length)
      answersStatus = answers
        .map(item => item.status)
        .reduce((prev, next) => prev + next)

    stats.total = Number(questionsStatus + answersStatus)
    if (stats.total === null) stats.total = 0
    user.stats = stats

    ctx.assert(user, 404)

    ctx.body = map(user)
  }

  async function addToFriends(ctx) {
    const { params, query } = validate(ctx, {
      params: joi
        .object({
          id: joi
            .string()
            .guid()
            .required()
        })
        .required(),
      query: joi.object({ add: joi.boolean().required() }).required()
    })

    ctx.assert(ctx.state.user.userId, 401)

    const friend = await usersCol.findOne({ _id: params.id })

    ctx.assert(friend, 404)

    let changeQuery = {}

    if (query.add) changeQuery = { $addToSet: { friends: friend._id } }
    else changeQuery = { $pull: { friends: friend._id } }

    const { value: updatedUser } = await usersCol.findOneAndUpdate(
      { _id: ctx.state.user.userId },
      changeQuery,
      { returnOriginal: false }
    )

    ctx.body = map(updatedUser)
  }

  async function subscribeToCat(ctx) {
    const { params, query } = validate(ctx, {
      query: joi.object({ category: joi.string().required() }).required()
    })

    ctx.assert(ctx.state.user.userId, 401)

    const user = await usersCol.findOne({ _id: ctx.state.user.userId })

    let changeQuery = {}

    if (!user.categories.includes(query.category))
      changeQuery = { $addToSet: { categories: query.category } }
    else changeQuery = { $pull: { categories: query.category } }

    const { value: updatedUser } = await usersCol.findOneAndUpdate(
      { _id: ctx.state.user.userId },
      changeQuery,
      { returnOriginal: false }
    )

    ctx.body = map(updatedUser)
  }

  async function remove(ctx) {
    ctx.assert(ctx.state.user.userId, 401)

    await usersCol.findOneAndUpdate(
      { _id: ctx.state.user.userId },
      {
        $set: {
          name: 'DELETED',
          email: 'DELETED',
          avatar:
            'https://zcoin.io/wp-content/uploads/2017/01/blank-avatar-300x300.png',
          friends: [],
          categories: [],
          updated: now()
          // providers left in case of users return
        }
      }
    )

    ctx.body = {}
    ctx.status = 204
  }

  function map(user) {
    return {
      id: user._id,
      name: user.name,
      avatar: user.avatar,
      friends: user.friends,
      categories: user.categories,
      stats: user.stats,
      created: user.created,
      updated: user.updated
    }
  }
}
