const Koa = require('koa')
const uuid = require('uuid/v4')
const cors = require('@koa/cors')
const koalogger = require('koa-logger')
const koaBody = require('koa-body')
const { MongoClient } = require('mongodb')
const bodyParser = require('koa-bodyparser')
const questions = require('./api/questions')
const questionStatus = require('./api/questionStatus')
const answers = require('./api/answers')
const answersStatus = require('./api/answerStatus')
const authenticationRoutes = require('./api/authentication')
const imagesRoutes = require('./api/images')
const errors = require('./errors')
const users = require('./api/users')
const TransientMap = require('./TransientMap')
const authorization = require('./authorization')
const logger = require('pino')()

async function server({
  port,
  dbUrl,
  jwtSecret,
  auth,
  usersMap = new TransientMap(),
  id = uuid,
  now = _ => new Date(),
  selfUrl = null
}) {
  try {
    const db = await MongoClient.connect(dbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(client => client.db(client.s.options.dbName))

    const authorize = authorization({ db, jwtSecret, usersMap })

    selfUrl = selfUrl || (() => `http://localhost:${port}`)

    const app = new Koa()
      .use(
        cors({
          exposeHeaders: ['ozo_pagination_total'],
          allowMethods: 'GET, POST, PATCH, OPTIONS, DELETE'
        })
      )
      .use(
        koaBody({
          formidable: {},
          multipart: true,
          urlencoded: true
        })
      )
      .use(koalogger())
      .use(errors())
      .use(bodyParser())
      .use(authenticationRoutes({ db, auth, usersMap, id, now }))
      .use(questionStatus({ db, authorize, id, now }))
      .use(
        questions({
          db,
          id,
          now,
          authorize
        })
      )
      .use(answers({ db, authorize, id, now }))
      .use(answersStatus({ db, authorize, id, now }))
      .use(users({ db, id, authorize, now }))
      .use(imagesRoutes({ db, id, authorize }))

    const httpServer = await new Promise((resolve, reject) => {
      const server = app
        .listen(port)
        .on('error', reject)
        .on('listening', _ => resolve(server))
    })

    return Promise.resolve({ db, httpServer })
  } catch (error) {
    logger.error(error)
    return Promise.reject(error)
  }
}

module.exports = server
