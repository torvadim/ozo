const server = require('./server')
const joi = require('@hapi/joi')
const logger = require('pino')()

const {
  APP_DB_URL,
  APP_PORT,
  JWT_SECRET,
  G_AUTH_CLIENT_ID,
  G_AUTH_CLIENT_SECRET,
  F_AUTH_CLIENT_ID,
  F_AUTH_CLIENT_SECRET
} = process.env

const { value, error } = joi.validate(
  {
    APP_DB_URL,
    APP_PORT,
    JWT_SECRET,
    G_AUTH_CLIENT_ID,
    G_AUTH_CLIENT_SECRET,
    F_AUTH_CLIENT_ID,
    F_AUTH_CLIENT_SECRET
  },
  {
    APP_DB_URL: joi.string().required(),
    APP_PORT: joi
      .number()
      .integer()
      .required(),
    JWT_SECRET: joi.string().required(),
    G_AUTH_CLIENT_ID: joi.string().required(),
    G_AUTH_CLIENT_SECRET: joi.string().required(),
    F_AUTH_CLIENT_ID: joi.string().required(),
    F_AUTH_CLIENT_SECRET: joi.string().required()
  }
)

const config = {
  dbUrl: value.APP_DB_URL,
  port: value.APP_PORT,
  jwtSecret: value.JWT_SECRET,
  auth: {
    jwtSecret: value.JWT_SECRET,
    gClientId: value.G_AUTH_CLIENT_ID,
    gClientSecret: value.G_AUTH_CLIENT_SECRET,
    fClientId: value.F_AUTH_CLIENT_ID,
    fClientSecret: value.F_AUTH_CLIENT_SECRET
  }
}
if (error) throw error

server(config)
  .then(() => logger.warn(`Listening on port ${config.port}`), config)
  .catch(err => {
    logger.level = 'error'
    console.log(err)
    logger.error('Server error', err)
  })
